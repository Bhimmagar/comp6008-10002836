﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App1
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
        void Handle_Clicked(Object sender, System.EventArgs e)
        {
            double counter = 0;
            double sum = 0;

            var numberEntered = new NumberClass();

            Button1.Text = "Done!";
            numberEntered.Num = double.Parse(UserEntry.Text);

            while (counter < numberEntered.Num)
            {
                if ((counter % 3) == 0)
                {
                    sum = sum + counter;
                }
                else if ((counter % 5) == 0)
                {
                    sum = sum + counter;
                }
                counter ++;
            }
            Label2.Text = $"The total of the multiples of 3 and 5 under a number {numberEntered.Num} is {sum}.";
            Label3.Text = $"Welcome to my calcultor";
        }


    }

}
